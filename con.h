#ifndef CON_H_INCLUDED
#define CON_H_INCLUDED

#include <Ecore.h>
#include <Ecore_Con.h>
#include <gsl>
#include "memory.h"

namespace con {

template <typename Event>
using EventCallback = std::function<void(const Event&)>;

template <typename Event>  // type can't be a parameter because EFL defines it non-const
class EventHandler {
public:
	using Callback = EventCallback<Event>;

	EventHandler(int type, Callback c)
	: eh {ecore_event_handler_add(type, adapter, this)}
	, callback {c} {
		if (!eh)
			throw std::runtime_error("can't add event handler");
	}

	~EventHandler() {
		if (eh)
			ecore_event_handler_del(eh);
	}

	EventHandler(EventHandler&) = delete;

	void callback_set(Callback c) {
		callback = c;
	}

private:
	static Eina_Bool adapter(void* data, int type, void* event) {
		auto self = reinterpret_cast<EventHandler*>(data);
		if (self->callback)
			self->callback(*reinterpret_cast<Event*>(event));
		return ECORE_CALLBACK_RENEW;
	}

	::Ecore_Event_Handler* eh {nullptr};
	Callback callback;
};

class Server {
public:
	Server(const std::string& path) {
		ecore_app_no_system_modules();
		ecore_init();
		ecore_con_init();
		srv = ecore_con_server_add(ECORE_CON_LOCAL_ABSTRACT, path.c_str(), 0, nullptr);
		if (!srv)
			throw std::runtime_error("can't create server");
	}

	~Server() {
		ecore_con_shutdown();
	}

	void add_callback_set(std::function<void()> fn) {
		add = std::make_unique<Handler<AddEvent>>(ECORE_CON_EVENT_CLIENT_ADD, srv,
			[fn](const AddEvent& e) {
				fn();
			}
		);
	}

	void del_callback_set(std::function<void()> fn) {
		del = std::make_unique<Handler<DelEvent>>(ECORE_CON_EVENT_CLIENT_DEL, srv,
			[fn](const DelEvent& e) {
				fn();
			}
		);
	}

	void data_callback_set(std::function<void(const gsl::span<const uint8_t>& s)> fn) {
		data = std::make_unique<Handler<DataEvent>>(ECORE_CON_EVENT_CLIENT_DATA, srv,
			[fn](const DataEvent& e) {
				fn(gsl::span<uint8_t> {
					reinterpret_cast<uint8_t*>(e.data),
					gsl::narrow_cast<unsigned>(e.size)
				});
			}
		);
	}

private:
	::Ecore_Con_Server* srv;

	template<typename Event>
	class Handler {
	public:
		Handler(int type, ::Ecore_Con_Server* srv, EventCallback<Event> c) {
			h = std::make_unique<EventHandler<Event>>(type,
				[srv, c](const Event& e) {
					auto event_srv = ecore_con_client_server_get(e.client);
					if (event_srv == srv)
						c(e);
				}
			);
		}

	private:
		std::unique_ptr<EventHandler<Event>> h;
	};

	using AddEvent  = Ecore_Con_Event_Client_Add;
	using DelEvent  = Ecore_Con_Event_Client_Del;
	using DataEvent = Ecore_Con_Event_Client_Data;

	std::unique_ptr<Handler<AddEvent>>  add;
	std::unique_ptr<Handler<DelEvent>>  del;
	std::unique_ptr<Handler<DataEvent>> data;

};

class Client {
public:
	Client(const std::string& path) {
		ecore_app_no_system_modules();
		ecore_init();
		ecore_con_init();
		srv = ecore_con_server_connect(ECORE_CON_LOCAL_ABSTRACT, path.c_str(), 0, nullptr);
	}

	~Client() {
		ecore_con_shutdown();
	}

	operator bool() const {
		return srv != 0;
	}

	void send(const std::string& s) {
		if (!srv)
			throw std::runtime_error("send() with no server connected");

		auto sent = ecore_con_server_send(srv, s.c_str(), s.length());
		ecore_con_server_flush(srv);
		if (sent == 0)
			throw std::runtime_error("error sending to server");
	}

	using AddEventHandler  = EventHandler<Ecore_Con_Event_Server_Add>;
	using DelEventHandler  = EventHandler<Ecore_Con_Event_Server_Del>;
	using DataEventHandler = EventHandler<Ecore_Con_Event_Server_Data>;

	void add_callback_set(AddEventHandler::Callback h) {
		add = std::make_unique<AddEventHandler>(ECORE_CON_EVENT_SERVER_ADD, h);
	}

	void del_callback_set(DelEventHandler::Callback h) {
		del = std::make_unique<DelEventHandler>(ECORE_CON_EVENT_SERVER_DEL, h);
	}

	void data_callback_set(DataEventHandler::Callback h) {
		data = std::make_unique<DataEventHandler>(ECORE_CON_EVENT_SERVER_DATA, h);
	}

private:
	::Ecore_Con_Server* srv;
	std::unique_ptr<AddEventHandler> add;
	std::unique_ptr<DelEventHandler> del;
	std::unique_ptr<DataEventHandler> data;
};

} // namespace

#endif // CON_H_INCLUDED
