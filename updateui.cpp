#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <string>
#include <vector>
#include <memory>
#include <algorithm>
#include <functional>
#include <iterator>
#include <iostream>
#include <sstream>
#include <chrono>
#include <thread>
#include "efl.h"
#include "con.h"

using namespace std;

string usage =
"Control the Update UI\n"
"\n"
"Usage:\n"
"  updateuictl [--message] [--percent <percent> | --seconds <seconds>] [--error]\n"
"  updateuictl --close\n"
"\n"
"Options:\n"
"  --message   accept a message on stdin\n"
"  --percent   set progress indicator to <percent>\n"
"  --seconds   indicate progress automatically for <seconds>\n"
"  --error     colorize or otherwise signify an error condition\n"
"  --close     close the interface\n";

vector<string> read_directory(const string& name)
{
	vector<string> entries;

	auto deleter = [](DIR* p){ closedir(p); };
	unique_ptr<DIR, decltype(deleter)> dirp {opendir(name.c_str()), deleter};
	if (!dirp) {
		throw runtime_error("cannot open " + name);
	}

	for (struct dirent* dp; (dp = readdir(dirp.get()));) {
		if (dp->d_name[0] != '.') {
			// only non-hidden entries
			entries.push_back(dp->d_name);
		}
	}

	return entries;
}

string find_logo(const string& libdir)
{
	// Return the lexicographically first file in our libdir.

	auto paths = read_directory(libdir);
	if (paths.empty()) {
		throw runtime_error("no files in " + libdir);
	}

	sort(paths.begin(), paths.end());
	return libdir + "/" + paths.front();
}

void updateui(const string& msg, unsigned seconds)
{
	efl::Ecore_Evas ee {"updateui"};
	ee.show();

	auto bg_rect = ee.rectangle_add();
	bg_rect.color_set(0, 0, 0, 255);
	ee.resize_object_add(bg_rect);
	bg_rect.show();

	auto logo_box = ee.box_add();
	ee.resize_object_add(logo_box);
	logo_box.show();
	auto path = find_logo("/lib/update-ui/logo.d");
	auto logo = ee.image_add(path.c_str());
	logo.resize_native(0.5);
	logo_box.append(logo);
	logo.show();

	auto status_box = ee.box_add();
	ee.resize_object_add(status_box);
	status_box.layout_vertical();
	status_box.align(0.5, 1.0);
	status_box.show();

	auto pbar_box = ee.box_add();
	status_box.append(pbar_box);
	pbar_box.layout_vertical();
	pbar_box.align(0.5, 0.95);
	pbar_box.size_hint_weight(1.0, 0.75);
	pbar_box.show();

	auto pbar = efl::make_progressbar(ee);
	pbar_box.append(pbar);
	pbar.size_hint_min_set(275, 6);
	pbar.show();

	auto text = ee.textblock_add();
	text.markup_set(msg.c_str());
	text.size_hint_min_set(400, 1);
	text.size_hint_weight(1.0, 0.25);
	status_box.append(text);
	text.show();

	con::Server close_server {"/var/run/updateui/close"};
	close_server.del_callback_set(
		[&ee]() {ee.loop_quit();}
	);

	efl::ArtificialProgress ap {pbar};
	if (seconds > 0) {
		ap.start(seconds);
	}

	con::Server p_server {"/var/run/updateui/percent"};
	p_server.data_callback_set(
		[&ap, &pbar](const gsl::span<const uint8_t>& s) {
			ap.cancel();
			pbar.progress(stod(string(s.begin(), s.end())));
		}
	);

	con::Server seconds_server {"/var/run/updateui/seconds"};
	seconds_server.data_callback_set(
		[&ap](const gsl::span<const uint8_t>& s) {
			auto sec = stod(string(s.begin(), s.end()));
			ap.start(sec);
		}
	);

	con::Server error_server {"/var/run/updateui/error"};
	error_server.del_callback_set(
		[&ap, &pbar]() {
			ap.cancel();
			pbar.progress(1.0);
			pbar.color_set(255, 0, 0, 255);
		}
	);

	con::Server message_server {"/var/run/updateui/message"};
	message_server.data_callback_set(
		[&text](const gsl::span<const uint8_t>& s) {
			text.markup_set(string(s.begin(), s.end()));
		}
	);

	ee.loop_begin();
}

template <typename F>
pid_t fork_daemon(const F &fn) {
	auto pid = ::fork();
	if (pid == 0) {
		daemon(0, 0);
		fn();
	}

	return pid;
}

class Options {
public:
	Options(int argc, char* argv[])
	: argv {argv, argv + argc} {
	}

	bool do_include(const string& s) {
		return find(argv.begin(), argv.end(), s) != argv.end();
	}

	template<typename T>
	T value_of(const string& o) {
		auto i = find(argv.begin(), argv.end(), o);
		if (i == argv.end() || ++i == argv.end())
			throw runtime_error("usage: no value after " + o);
		T v;
		istringstream s {*i};
		s >> v;
		if (!s)
			throw runtime_error("usage: can't convert value of " + o);
		return v;
	}

private:
	vector<string> argv;
};

int main(int argc, char* argv[]) {
	Options options {argc, argv};

	if (options.do_include("--close")) {
		con::Client {"/var/run/updateui/close"};
		return 0;
	}

	if (options.do_include("--percent")) {
		auto p = options.value_of<double>("--percent");
		con::Client p_server {"/var/run/updateui/percent"};
		p_server.send(to_string(p));
	}

	if (options.do_include("--error")) {
		con::Client {"/var/run/updateui/error"};
	}

	string msg {"Installing Software Update"};
	if (options.do_include("--message")) {
		cin.unsetf(ios_base::skipws);
		msg.assign(istream_iterator<char>(cin), istream_iterator<char>());
	}

	unsigned seconds = 0;
	if (options.do_include("--seconds")) {
		seconds = options.value_of<unsigned>("--seconds");
	}

	string const msg_path {"/var/run/updateui/message"};
	con::Client msg_server {msg_path};
	if (!msg_server) {
		if (options.do_include("--foreground")) {
			updateui(msg, seconds);
		} else {
			fork_daemon([=,&msg]{updateui(msg, seconds);});

			// wait for server to start before exiting
			while (! con::Client {msg_path}) {
				this_thread::sleep_for(chrono::milliseconds(100));
			};
		}

		return 0;
	}

	if (options.do_include("--message")) {
		msg_server.send(msg);
	}

	if (options.do_include("--seconds")) {
		con::Client s {"/var/run/updateui/seconds"};
		s.send(to_string(seconds));
	}

	return 0;
}
