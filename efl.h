#ifndef EFL_H_INCLUDED
#define EFL_H_INCLUDED

#include <Ecore.h>
#include <Evas.h>
#include <Ecore_Evas.h>
#include <vector>
#include <functional>
#include "memory.h"
#include <stdexcept>

namespace efl {

class Evas_Object {
public:
	Evas_Object(::Evas_Object* _eo)
	: eo {_eo} {
		if (!_eo)
			throw std::runtime_error("Evas_Object creation error");
	}

	void show() {
		evas_object_show(eo);
	}

	void resize(int w, int h) {
		evas_object_resize(eo, w, h);
	}

	void size_hint_min_set(int w, int h) {
		evas_object_size_hint_min_set(eo, w, h);
	}

	void size_hint_weight(double w, double h) {
		evas_object_size_hint_weight_set(eo, w, h);
	}

	void size_hint_padding(int left, int right, int top, int bottom) {
		evas_object_size_hint_padding_set(eo, left, right, top, bottom);
	}

	void geometry_get(int* x, int* y, int* w, int* h) {
		evas_object_geometry_get(eo, x, y, w, h);
	}

	std::tuple<int,int,int,int> geometry() {
		int x, y, w, h;
		evas_object_geometry_get(eo, &x, &y, &w, &h);
		return std::make_tuple(x, y, w, h);
	}


	void geometry_set(int x, int y, int w, int h) {
		evas_object_resize(eo, w, h);
		evas_object_move(eo, x, y);
	}

	void color_set(int r, int g, int b, int a) {
		evas_object_color_set(eo, r, g, b, a);
	}

	void stack_above(::Evas_Object* other) {
		evas_object_stack_above(eo, other);
	}

	operator ::Evas_Object*() {
		return eo;
	}

	::Evas_Object* get_evas_object() {
		return eo;
	}

private:
	::Evas_Object* eo {nullptr};
};

class Rectangle : public Evas_Object {
public:
	Rectangle(::Evas_Object* eo)
	: Evas_Object {eo} {
	}
};

class Box : public Evas_Object {
public:
	Box(::Evas_Object* eo)
	: Evas_Object {eo} {
	}

	void append(Evas_Object &o) {
		auto result = evas_object_box_append(*this, o);
		if (!result)
			throw std::runtime_error("error appending to box");
	}

	void layout_vertical() {
		evas_object_box_layout_set(
			*this,
			evas_object_box_layout_vertical,
			nullptr,
			nullptr
		);
	}

	void layout_homogeneous_vertical() {
		evas_object_box_layout_set(
			*this,
			evas_object_box_layout_homogeneous_vertical,
			nullptr,
			nullptr
		);
	}

	void align(double h, double v) {
		evas_object_box_align_set(*this, h, v);
	}

	void padding(int hor, int ver) {
		evas_object_box_padding_set(*this, hor, ver);
	}
};

class Image: public Evas_Object {
public:
	Image(::Evas_Object* eo)
	: Evas_Object {eo} {
	}

	void file_set_filled(const std::string& filename) {
		evas_object_image_file_set(*this, filename.c_str(), nullptr);
		evas_object_image_filled_set(*this, true);
	}

	void resize_native(double scale=1.0) {
		int w, h;
		evas_object_image_size_get(*this, &w, &h);
		check_error();
		resize(w * scale, h * scale);
	}

private:
	void check_error() {
		auto e = evas_object_image_load_error_get(*this);
		switch (e) {
		case EVAS_LOAD_ERROR_NONE:
			return;
		default:
			auto s = evas_load_error_str(e);
			throw std::runtime_error("error loading image: " + std::string(s));
		}
	}
};

class Textblock : public Evas_Object {
public:
	Textblock(::Evas_Object* o)
	: Evas_Object {o} {
		auto st = evas_textblock_style_new();
		evas_textblock_style_set(st, "DEFAULT='"
			"font=Sans "
			"font_size=18 "
			"font_weight=thin "
			"color=#ffffff "
			"align=center "
			"valign=top "
			"wrap=word'");
		evas_object_textblock_style_set(*this, st);
	}

	void markup_set(const std::string& s) {
		evas_object_textblock_text_markup_set(*this, s.c_str());
	}
};

class Ecore_Evas {
public:
	Ecore_Evas(const std::string& title = std::string()) {
		ecore_app_no_system_modules();
		auto rc = ecore_evas_init();
		if (rc <= 0)
			throw std::runtime_error("can't init ecore_evas");

		ee = ecore_evas_new(nullptr, 0, 0, 800, 480, nullptr);
		if (!ee)
			throw std::runtime_error("can't create ecore_evas");

		ecore_evas_fullscreen_set(ee, true);

		if (!title.empty())
			ecore_evas_title_set(ee, title.c_str());

		ecore_evas_data_set(ee, "self", this);
		ecore_evas_callback_resize_set(ee, resize_callback);
	}

	~Ecore_Evas() {
		ecore_evas_free(ee);
		ecore_evas_shutdown();
	}

	void show() {
		ecore_evas_show(ee);
	}

	void resize(int w, int h) {
		ecore_evas_resize(ee, w, h);
	}

	static void resize_callback(::Ecore_Evas *e) {
		auto self = reinterpret_cast<Ecore_Evas*>(ecore_evas_data_get(e, "self"));
		for (auto r : self->resize_objects)
			self->resize_object_resize(*r);
	}

	// Convenience function for starting the associated main loop.
	void loop_begin() {
		ecore_main_loop_begin();
	}

	// Convenience function for quitting the associated main loop.
	void loop_quit() {
		ecore_main_loop_quit();
	}

	::Evas* get_evas() {
		auto evas = ecore_evas_get(ee);
		if (!evas)
			throw std::runtime_error("can't get evas");

		return evas;
	}

	void resize_object_add(Evas_Object &o) {
		resize_objects.push_back(&o);
		resize_object_resize(o);
	}

	void resize_object_resize(Evas_Object& o) {
		int w, h;
		ecore_evas_geometry_get(ee, nullptr, nullptr, &w, &h);
		o.resize(w, h);
	}

	Rectangle rectangle_add() {
		auto e = get_evas();
		Rectangle r {evas_object_rectangle_add(e)};
		return std::move(r);
	}

	Box box_add() {
		auto e = get_evas();
		return Box {evas_object_box_add(e)};
	}

	Image image_add(const std::string &filename) {
		auto e = get_evas();
		Image i {evas_object_image_add(e)};
		i.file_set_filled(filename);
		return std::move(i);
	}

	Textblock textblock_add() {
		auto e = get_evas();
		return Textblock {evas_object_textblock_add(e)};
	}

private:
	::Ecore_Evas *ee {nullptr};
	std::vector<Evas_Object*> resize_objects;
};

class ProgressBar : public Evas_Object {
public:
	ProgressBar(Rectangle&& _r1, Rectangle&& _r2)
	: Evas_Object {_r1}
	, r2 {std::move(_r2)} {
		r2.color_set(0, 0, 0, 255);
		r2.show();
		r2.stack_above(*this);

		evas_object_event_callback_add(*this, EVAS_CALLBACK_RESIZE, resize_cb, this);
		if (evas_alloc_error() != EVAS_ALLOC_ERROR_NONE)
			throw std::runtime_error("error setting callback on ProgressBar");

		evas_object_event_callback_add(*this, EVAS_CALLBACK_MOVE, resize_cb, this);
		if (evas_alloc_error() != EVAS_ALLOC_ERROR_NONE)
			throw std::runtime_error("error setting callback on ProgressBar");
	}

	ProgressBar(ProgressBar&& o)
	: Evas_Object {o}
	, r2 {std::move(o.r2)} {
		evas_object_event_callback_add(*this, EVAS_CALLBACK_RESIZE, resize_cb, this);
		if (evas_alloc_error() != EVAS_ALLOC_ERROR_NONE)
			throw std::runtime_error("error move-copying callback ProgressBar");

		evas_object_event_callback_add(*this, EVAS_CALLBACK_MOVE, resize_cb, this);
		if (evas_alloc_error() != EVAS_ALLOC_ERROR_NONE)
			throw std::runtime_error("error move-copying callback on ProgressBar");
	}

	~ProgressBar() {
		evas_object_event_callback_del_full(*this, EVAS_CALLBACK_RESIZE, resize_cb, this);
		evas_object_event_callback_del_full(*this, EVAS_CALLBACK_MOVE, resize_cb, this);
	}

	void progress(double _p) {
		p = _p;
		resize_r2();
	}

	double progress() const {
		return p;
	}

private:
	static void resize_cb(void *data, ::Evas *e, ::Evas_Object *r1, void *event_info) {
		auto self = reinterpret_cast<ProgressBar*>(data);
		self->resize_r2();
	}

	void resize_r2() {
		int x, y, w, h;
		geometry_get(&x, &y, &w, &h);
		x = x + b + p * (w - 2 * b);
		y = y + b;
		w = w - 2 * b - p * (w - 2 * b);
		h = h - 2 * b;

		r2.geometry_set(x, y, w, h);
	}

	static constexpr int b {1};
	double p {0.0};
	Rectangle r2;
};

ProgressBar make_progressbar(Ecore_Evas& e) {
	ProgressBar p(e.rectangle_add(), e.rectangle_add());
	return std::move(p);
}

class Animator {
public:
	using delegate_fn = std::function<bool(double)>;

	Animator(double runtime, delegate_fn d)
	: delegate {d}
	, animator {ecore_animator_timeline_add(runtime, cb, this)} {
	}

	~Animator() {
		if (animator) {
			ecore_animator_del(animator);
			animator = nullptr;
		}
	}

private:
	static Eina_Bool cb(void *data, double pos) {
		auto self = reinterpret_cast<Animator*>(data);

		if (pos == 1.0) {
			// ecore has deleted the animator
			self->animator = nullptr;
		}

		return self->delegate(pos);
	}

	delegate_fn delegate;
	Ecore_Animator* animator;
};

class ArtificialProgress {
public:
	ArtificialProgress(ProgressBar& _bar)
	: bar (_bar)
	{ }

	bool animate(double pos) {
		bar.progress(initial + pos * (1.0 - initial));
		return true;
	}

	void start(double seconds) {
		initial = bar.progress();
		animator = std::make_unique<Animator>(seconds,
			[this](double pos) {return animate(pos);});
	}

	void cancel() { animator.reset(); }

private:
	ProgressBar& bar;
	std::unique_ptr<Animator> animator;
	double initial {0.0};
};

} // namespace efl

#endif // EFL_H_INCLUDED
