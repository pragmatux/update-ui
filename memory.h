#ifndef MEMORY_H_INCLUDED
#define MEMORY_H_INCLUDED

#include <memory>

namespace std {

template<typename T, typename... Args>
unique_ptr<T> make_unique(Args&&... args) {
	return unique_ptr<T>(new T(forward<Args>(args)...));
}

}

#endif // MEMORY_H_INCLUDED
